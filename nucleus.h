#define NU_MAX_NAME 8
#define PAD_1 1;
#define PAD_3 1;

typedef uint32_t UNSIGNED;
typedef int32_t SIGNED;
typedef unsigned char OPTION;
typedef OPTION DATA_ELEMENT;
typedef char CHAR;
typedef int32_t INT;
typedef void VOID;
typedef *uint32_t UNSIGNED_PTR;
typedef *DATA_ELEMENT BYTE_PTR;
typedef int32_t STATUS;


typedef struct CS_NODE_STRUCT
{
	struct CS_NODE_STRUCT	*cs_previous;
	struct CS_NODE_STRUCT	*cs_next;
	DATA_ELEMENT	cs_priority;
	DATA_ELEMENT	cs_padding[PAD_1];
} CS_NODE;

VOID CSC_Place_On_List(CS_NODE **head, CS_NODE *new_node);
VOID CSC_Priority_Place_On_List(CS_NODE **head, CS_NODE *new_node);
VOID CSC_Remove_From_List(CS_NODE **head, CS_NODE *node);


VOID INT_Initialize(void);
VOID INC_Initialize(VOID *first_available_memory);
INT INT_Vectors_Loaded(void);

VOID *INT_Setup_Vector(INT vector, VOID *new);

typedef struct TC_PROTECT_STRUCT
{
	TC_TCB	*tc_tcb_pointer;
	UNSIGNED tc_thread_waiting;
} TC_PROTECT;

typedef TC_PROTECT TCD_List_Protect;
typedef TC_PROTECT TCD_System_Protect;
typedef TC_PROTECT TCD_LISR_Protect;
typedef	TC_PROTECT TCD_HISR_Protect;

typedef	struct TC_TCB_STRUCT
{
	CS_NODE			tc_created;
	UNSIGNED		tc_id;
	CHAR			tc_name[NU_MAX_NAME];
	DATA_ELEMENT	tc_status;
	DATA_ELEMENT	tc_delayed_suspend;
	DATA_ELEMENT	tc_priority;
	DATA_ELEMENT	tc_preemption;
	UNSIGNED		tc_scheduled;
	UNSIGNED		tc_cur_time_slice;
	void			*tc_stack_start;
	void			*tc_stack_end;
	void			*tc_stack_pointer;
	UNSIGNED		tc_stack_size;
	UNSIGNED		tc_stack_minimum;
	struct TC_PROTECT		*tc_current_protect;
	void			*tc_saved_stack_ptr;
	UNSIGNED		tc_stack_size;
	struct TC_TCB_STRUCT *tc_ready_previous;
	struct TC_TCB_STRUCT *tc_ready_next;
	UNSIGNED		tc_priority_group;
	struct TC_TCB_STRUCT	**tc_priority_head;
	DATA_ELEMENT	*tc_sub_priority_ptr;
	DATA_ELEMENT	tc_sub_priority;
	DATA_ELEMENT	tc_saved_status;
	DATA_ELEMENT	tc_signal_active;
	DATA_ELEMENT	tc_padding[PAD_3];
	void			(*tc_entry)(UNSIGNED, void *);
	UNSIGNED		tc_argc;
	void			*tc_argv;
	void			(*tc_cleanup)(void *);
	void			*tc_cleanup_info;
	struct TC_PROTECT_STRUCT *tc_suspend_protect;
	INT				tc_timer_active;
	TM_TCB			tm_timer_control;
	UNSIGNED		tc_signals;
	UNSIGNED		tc_enabled_signals;
	VOID			(*tc_signal_handler)(UNSIGNED);
	UNSIGNED		tc_system_reserved_1;
	UNSIGNED		tc_system_reserved_2;
	UNSIGNED		tc_system_reserved_3;
	UNSIGNED		tc_app_reserved_1;
} TC_TCB;

typedef	struct TC_HCB_STRUCT
{
	CS_NODE			tc_created;
	UNSIGNED		tc_id;
	CHAR			tc_name[8];
	DATA_ELEMENT	tc_not_used_1;
	DATA_ELEMENT	tc_not_used_2;
	DATA_ELEMENT	tc_priority;
	DATA_ELEMENT	tc_not_used_3;
	UNSIGNED		tc_scheduled;
	UNSIGNED		tc_cur_time_slice;
	void			*tc_stack_start;
	void			*tc_stack_end;
	void			*tc_stack_pointer;
	UNSIGNED		tc_stack_size;
	UNSIGNED		tc_stack_minimum;
	struct TC_PROTECT_STRUCT *tc_current_protect;
	struct TC_HCB_STRUCT	*tc_active_next;
	UNSIGNED		tc_activationn_count;
	void			(*tc_entry)(void);
	UNSIGNED		tc_system_reserved_1;
	UNSIGNED		tc_system_reserved_2;
	UNSIGNED		tc_system_reserved_3;
	UNSIGNED		tc_app_reserved_1;
} TC_HCB;

typedef	NU_TASK	TC_TCB;
typedef NU_HISR TC_HCB;

STATUS TCC_Create_Task(NU_TASK *task_ptr, CHAR *name, VOID (*task_entry)(UNSIGNED, VOID *), UNSIGNED argc, VOID *argv, VOID *stack_address, UNSIGNED stack_size, OPTION priority,UNSIGNED time_slice, OPTION preempt, OPTION auto_start);
STATUS TCC_Delete_Task(NU_TASK *task_ptr);
STATUS TCC_Reset_Task(NU_TASK *task_ptr, UNSIGNED argc, VOID *argv);
STATUS TCC_Terminate_Task(NU_TASK *task_ptr);

STATUS TCC_Resume_Task(NU_TASK *task_ptr, OPTION suspend_type);

STATUS TCC_Create_HISR(NU_HISR *hisr_ptr, CHAR *name, VOID (*hisr_entry)(VOID), OPTION priority, VOID *stack_address,UNSIGNED stack_size);
STATUS TCC_Delete_HISR(NU_HISR *hisr_ptr);

STATUS TCC_Resume_Service(NU_TASK *task_ptr);

VOID TCC_Suspend_Task(NU_TASK *task_ptr, OPTION suspend_type, VOID (*cleanup) (VOID*),VOID*information, UNSIGNED timeout);

STATUS TCC_Suspend_Service(NU_TASK *task_ptr);
VOID TCC_Task_Timeout(NU_TASK *task_ptr);
VOID TCC_Task_Sleep(UNSIGNED ticks);
VOID TCC_Relinquish(VOID);
VOID TCC_Time_Slice(NU_TASK *task_ptr);

NU_TASK *TCC_Current_Task_Pointer(VOID);
NU_HISR *TCC_Current_HISR_Pointer(VOID);
VOID TCC_Task_Shell(VOID);
VOID TCC_Signal_Shell(VOID);
VOID TCC_Dispatch_LISR(INT vector);

STATUS TCC_Register_LISR(INT vector, VOID(*new_lisr)(INT), VOID (**old_lisr)(INT))

STATUS TCCE_Create_Task(NU_TASK *task_ptr, CHAR *name, VOID (*task_entry)(UNSIGNED,VOID*),UNSIGNED argc, VOID *argv, VOID *stack_address, UNSIGNED stack_size, OPTION priority, UNSIGNED time_slice, OPTION preempt, OPTION auto_start);
STATUS TCCE_Create_HISR(NU_HISR *hisr_ptr, CHAR *name, VOID (*hisr_entry)(VOID), OPTION priority, VOID *stack_address, UNSIGNED stack_size);
STATUS TCCE_Delete_HISR (NU_HISR *hisr_ptr);
STATUS TCCE_Delete_Task(NU_TASK *task_ptr);
STATUS TCCE_Reset_Task(NU_TASK* task_ptr,UNSIGNED argc, VOID *argv);
STATUS TCCE_Terminate_Task(NU_TASK *task_ptr);
STATUS TCCE_Resume_Service(NU_TASK *task_ptr);
STATUS TCCE_Suspend_Service(NU_TASK *task_ptr);
VOID TCCE_Relinquish(VOID);
VOID TCCE_Task_Sleep(UNSIGNED ticks);
INT TCCE_Suspend_Error(VOID);
STATUS TCCE_Activate_HISR(NU_HISR *hisr_ptr);
STATUS TCCE_Validate_Resume(OPTION resume_type, NU_TASK *task_ptr);

UNSIGNED TCF_Established_Tasks(VOID);
UNSIGNED TCF_Established_HISRs(VOID);
UNSIGNED TCF_Task_Pointers(NU_TASK **pointer_list, UNSIGNED maximum_pointers);
UNSIGNED TCF_HISR_Pointers(NU_HISR **pointer_list, UNSIGNED maximum_pointers);
STATUS TCF_Task_Information(NU_TASK *task_ptr, CHAR *name, DATA_ELEMENT *status, UNSIGNED *scheduled_count, DATA_ELEMENT *priority, OPTION *preempt, UNSIGNED *time_slice, VOID **stack_base, UNSIGNED *stack_size, UNSIGNED *minimum_stack);
STATUS TCF_HISR_Information(NU_HISR *hisr_ptr, CHAR *name, UNSIGNED *scheduled_count, DATA_ELEMENT *priority, VOID **stack_base, UNSIGNED *stack_size, UNSIGNED minimum_stack);

VOID TCI_Initialize(VOID);

OPTION TCS_Change_Priority(NU_TASK *task_ptr, OPTION new_priority);
OPTION TCS_Change_Preemption(OPTION preempt);
UNSIGNED TCS_Change_Time_Slice(NU_TASK *task_ptr, UNSIGNED time_slice);
UNSIGNED TCS_Control_Signals(UNSIGNED enable_signal_mask);
UNSIGNED TCS_Receive_Signals(VOID);
STATUS TCS_Register_Signal_Handler(VOID (*signal_handler) (UNSIGNED));
STATUS TCS_Send_Signals(NU_TASK *task_ptr, UNSIGNED signals);

OPTION TCSE_Change_Priority(NU_TASK *task_ptr, OPTION new_priority);
OPTION TCSE_Change_Preemption(OPTION preempt);
UNSIGNED TCSE_Change_Time_Slice(NU_TASK *task_ptr, UNSIGNED time_slice);
UNSIGNED TCSE_Control_Signals(UNSIGNED enable_signal_mask);
UNSIGNED TCSE_Receive_Signals(VOID);
STATUS TCSE_Register_Signal_Handler(VOID (*signal_handler) (UNSIGNED));
STATUS TCSE_Send_Signals(NU_TASK *task_ptr, UNSIGNED signals);

INT TCT_Control_Interrupts(new_level);
INT TCT_Local_Control_Interrupts(new_level);
VOID TCT_Restore_Interrupts(VOID);
VOID TCT_Build_Task_Stack(TC_TCB *task);
VOID TCT_Build_HISR_Stack(TC_HCB *hisr);
VOID TCT_Build_Signal_Frame(TC_TCB *task);
UNSIGNED TCT_Check_Stack(void);
VOID TCT_Schedule(void);
VOID TCT_Control_To_Thread(TC_TCB *thread);
VOID TCT_Control_To_System(void);
VOID TCT_Signal_Exit(void);
VOID *TCT_Current_Thread(void);
VOID TCT_Set_Execute_Task(TC_TCB *task);
VOID TCT_Protect(TC_PROTECT *protect);
VOID TCT_Unprotect(void);
VOID TCT_Unprotect_Specific(TC_PROTECT *protect);
VOID TCT_Set_Current_Protect(TC_PROTECT *protect);
VOID TCT_Protect_Switch(VOID *thread);
VOID TCT_Schedule_Protected(VOID *thread);
VOID TCT_Interrupt_Context_Save(vector);
VOID TCT_Interrupt_Context_Restore(void);
STATUS TCT_Activate_HISR(TC_HCB *hisr);
VOID TCT_HISR_Shell(void);
VOID TCT_Check_For_Preemption(void);

// Page 77