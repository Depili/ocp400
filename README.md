# Reverse engineering Thomson / Grass Valley OCP 400 camera controller

Following is from firmware version 41.

## Firmware initialization

1. Boot loader, currently unknown
2. Protected mode initialization at 0x03ff0000
  1. Sets up GTD and IDT in flash
    * GTD at 0x03ff0080
      * Entries at 0x03ff0c98
    * IDT at 0x03ff0086
      * Entries at 0x03ff0d80
3. 32bit mode initialization at 0x03ff0800
  1. Masks the first 64k to another CS line
    * Hides the bootloader, replaces with RAM??
4. Rom-Monitor init
  * Copies GDT and IDT to ram
  * Initializes serial at 0xf8f8 == UART 1 / Com2 ()
  * Initializes the interrupt controllers (0x03c42108):
    * IRQ:s will be:
      * IRQ 8  0x70 INT4
      * IRQ 9  0x71 INT5 / SSIOINT?
      * IRQ 10 0x72 TMROUT1
      * IRQ 11 0x73 TMROUT2
      * IRQ 12 0x74
      * IRQ 13 0x75 INT6 - I2C
      * IRQ 14 0x76
      * IRQ 15 0x77 WDTOUT# - Spurious interrupt handler
      * IRQ 0  0x78 TMROUT0 (Nucleos system timer)
      * IRQ 1  0x79
      * IRQ 2  0x7A Slave cascade
      * IRQ 3  0x7B SIOINT1
      * IRQ 4  0x7C SIOINT0
      * IRQ 5  0x7D INT1
      * IRQ 6  0x7E INT2 - Class D / Procio
      * IRQ 7  0x7F INT3 - Spurious interrupt handler
  * Initialize Task Status Segment at 0x03ff1000
  * Jump to the TSS
5. Nucleus RTOS Init via Task Status Segment (0x03c4f871)
  * Timer0 initialization
6. Application initialization


## App serial ports

* Port 0 = 0xf4f8 (uart 0 aka COM1)
* Port 1 = 0xf8f8 (uart 1 aka COM2) - Serial Monitor
* Port 2 = 0x3e8
* Port 3 = 0x2e8


## Memory layout

* 0x00000000 - Start of RAM
* 0x001fffff - End of RAM
* 0x01000000 - NVRAM?
* 0x0100ffff - NVRAM end
* 0x02000000 - Memory mapped IO with the FPGA
* 0x03800000 - Start of flash bank1 (8Mx8 Am29DL640D)
* 0x0380000C - App status location?
* 0x03802000 - FPGA ID 1
* 0x03802002 - FPGA ID 2
* 0x03802004 - FPGA size location
* 0x03802008 - FPGA DL location
* 0x03840000 - App ID 1
* 0x03840002 - App ID 2
* 0x03840008 - App DL
* 0x03900000 - Start of flash bank2
* 0x03c00000 - Start of flash bank3
* 0x03C42000 - App rom start
* 0x03c42100 - Start of the serial monitor code
* 0x03c4f871 - Start of Nucleus code
* 0x03c58fec - Start of application code
* 0x03d5b2f0 - End of application code
* 0x03f00000 - Start of flash bank4
* 0x03ff0000 - Protected mode initialization code
* 0x03ff1000 - Task Status Segment
* 0x03ff1ff8 - App rom end
* 0x03ffffff - End of address space

* 0x00020000 - 0x0005dff0 - FPGA Rom?


## Memory mapped io

Block starting at `0x02000000` is for memory mapped IO via the FPGA. Addresses bellow are offsets to the region.

If address `0x2000030` is > `0x18` or address `0x2000031` is not 0 the hardware drives the LCD and matrix screens directly.

### Hitachi LCD

Either available directly at `0x17D`, 20 bytes per row or shifted out manually:

* 0x42 - LCD control pins
  * 0x01 - E
  * 0x02 - RW
  * 0x04 - RS
* 0x43 - LCD data bus

CMD write:
1. Set 0x42 to 0x00
2. Write data to 0x43
3. Delay
4. Set 0x42 to 0x01
5. Set 0x42 to 0x00

Data write:
1. Set 0x42 to 0x04
2. Write data to 0x043
3. Set 0x42 to 0x05
4. Delay
5. Set 0x42 to 0x4
6. Delay

### SPI

* 0x04 - SPI data register, 8bits
* 0x05 - SPI data register2 for 16bit ops, set bit 0x02 on the CMD register
* 0x06 - SPI CS and command register
  * CS lines mask: 0111 0000, 8 CS lines as a bitfield
  * Set CS command bit: 0x04
  * 16bit data flag: 0x02
  * Output 8bits cmd bit: 0x01
  * Done flag: 0x80, polled with 1000 count loop


#### Write 8bit

1. Set CS selection and bit 0x04 in 0x06
2. Write byte to 0x04
3. Set bit 0x01 in 0x06, keep others
4. Poll bit 0x80 in 0x06. Use count 1000 and end of loop is an error
  1. Read 0x06
  2. and 0x80, if 0x80 break
  3. Goto 1
5. Loop to 2 until all bytes are sent
6. Clear all bits except 0x70 in 0x06


#### Read 8bit

Same as 8bit write, but write out 0x00 bytes and read from 0x04 after the polling


#### 8bit transaction

1. Set CS selection and bit 0x04 in 0x06
2. Write data to 0x04
3. Set bit 0x01 in 0x06, keep others
4. Poll ready bit
5. Read data from 0x04
6. goto 2 until transfer complete
7. Clear all bits except 0x70 in 0x06


#### 16bit transaction

1. Set CS selection and bit 0x04 in 0x06
2. Write data to 0x04 and 0x05
3. Set bits 0x03 in 0x06, keep others
4. Poll ready bit
5. Read data from 0x04 and 0x05
6. goto 2 until transfer complete
7. Clear all bits except 0x70 in 0x06


#### ADC

* SPI bus 0
* Channels 1-4 are used
* Read commands:
  * Chan 0: 0x8E
  * Chan 1: 0xCE
  * Chan 2: 0x9E
  * Chan 3: 0xDE
  * Chan 4: 0xAE
  * Chan 5: 0xEE
  * Chan 6: 0xBE
  * Chan 7: 0xFE
* Do SPI 8bit transaction with sending `[READ_CMD, 0x00, 0x00]` and get the value in bytes 1 and 2.


### I2C

* 0x02 - interrupt stuff?
* 0x03 - interrupt stuff?


#### IO expanders


#### RTC


#### EEPROM


### Matrix displays

Either driven by the HW directly or via the following code

* 0x40 - Shift register IO
  * 0x01 - CS0 - Big display
  * 0x02 - CS1 - Small display
  * 0x04 - Data out
  * 0x08 - CLK
  * 0x10 - CMD
* Small display direct registers: `0x101` - `0x114`
* Large display direct registers: `0x115` - `0x13C`

Initialization commands:
* Shift zeros to the devices
* Write command 0x4C and 0x80 to the small display
* Write command 0x4C and 0x80 to the big display

Command write:
1. Set CLK and CMD bits
2. Clear appropriate CS bit
3. Clear CLK bit
4. Loop 8 times
  1. Set data out bit per MSB bit of the data byte
  2. Set CLK bit
  3. Delay
  4. shift data left by 1 bit
  5. clear CLK bit
5. Set appropriate CS bits

Data write:
1. Set CLK bit
2. Clear CMD bit
3. Clear appropriate CS bit
4. Clear the CLK bit
5. Loop 20 or 40 times
  1. Loop 8 times:
    1. Set data out bit per MSB bit of data byte
    2. Set CLK bit
    3. delay
    4. shift data left by 1 bit
    5. clear CLK bit
6. Set the appropriate CS bit


### Encoders

* 8 bytes starting from 0x200
* 1 byte per encoder


### Led PWM registers

* 0x100 - 1 byte, 1 register, only available if `0x30` > 18 or `0x31` != 0
* 0x210 - 6 bytes, 1 byte per register
* 0x220 - 1 byte, 1 register
* 0x224 - 6 bytes, 1 byte per register


### Leds

* 0x221


### Buttons

* 0x222


### Card


### Misc

* 0x145
  * 16 byte initialization write, unknown device
  * Only available with 0x30 > 18 or 0x31 != 0
  * Init data: 0x17, 0x1F, 0x1F, 0x17, 0x02, 0x05, 0x05, 0x,00, 0x1E, 0x12, 0x12, 0x1E, 0x1E, 0x1E, 0x1E, 0x00
* 0x155, 0x15D, 0x165, 0x16D, 8 byte things
  * Initialized to zeroes
  * Color Correction related?
  * Only available with 0x30 > 18 or 0x31 != 0


## Ethernet controller

