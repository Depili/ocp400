package main

import (
	"bufio"
	"fmt"
	"os"
)

const (
	seek   = 0
	packet = iota
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var out *os.File

func main() {

	f, err := os.Open(os.Args[1])
	check(err)

	out, err = os.OpenFile(os.Args[2], os.O_CREATE|os.O_RDWR, 0644)
	check(err)

	r := bufio.NewReader(f)

	buffer := make([]byte, 0)
	state := seek

	for {
		b, err := r.ReadByte()

		if err != nil {
			fmt.Printf("-> Read error, giving up\n")
			break
		}

		if (b & 0x80) == 0x80 {
			if state == packet {
				fmt.Printf("-> Error: high bit set inside packet!\n")
			}

			buffer = make([]byte, 0)
			buffer = append(buffer, b)
			state = packet
			continue
		}

		if b == 0x0D {
			if state == packet {
				printPacket(buffer)
			} else {
				fmt.Printf("-> Spurious 0x0D!\n")
			}
			state = seek
			continue
		}

		if b < 0x20 {
			fmt.Printf("-> Extra control byte: 0x%02X\n", b)
			continue
		}

		if state != seek {
			buffer = append(buffer, b)
		}

	}

	f.Close()
}

func printPacket(b []byte) {

	if b[0] == 0xAE {
		// Ack
		p1 := []byte{0xAE, 0x4F, 0x4B, 0x22, 0x3A, 0x5F, 0x20, 0x2C}
		for i := range b {
			if b[i] != p1[i] {
				fmt.Printf("Unknown ACK len: %d: ", len(b))
				printHeader(b)
				data := parseData(b)
				fmt.Printf(" %X\n", data)
				return
			}
		}
		fmt.Printf("-> Write OK\n")
		return
	}

	// fmt.Printf("Packet: %02X %c %c %c %c %c %c %c len: %d\n", b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7], len(b))

	if b[0] == 0xC8 && writePacket(b) {
		fmt.Printf("Write: %3d byte packet ", len(b))
		printHeader(b)
		fmt.Printf(" data: ")
		printData(b)
		return
	}
	if b[1] == 0x57 {

	}
	fmt.Printf("Unknown packet len: %0d ", len(b))
	printHeader(b)
	data := parseData(b)
	fmt.Printf(" %X\n", data)
}

var prevAddr int = 0
var prevSize int = 0

func writePacket(b []byte) bool {
	if b[2] != 0x4C {
		return false
	}

	return true

	switch b[1] {
	case 0x27:
		return true
	case 0x2f:
		return true
	case 0x37:
		return true
	case 0x3f:
		return true
	case 0x47:
		return true
	case 0x4f:
		if b[3] == 0x2f || b[3] == 0x2b || b[3] == 0x27 {
			return true
		}
	case 0x57:
		if b[3] == 0x2f || b[3] == 0x2b || b[3] == 0x27 {
			return true
		}
	case 0x5f:
		return true
	}

	return false
}

func printHeader(b []byte) {
	fmt.Printf("-> 0x%02X ", b[0])
	for i := 1; i < 4; i++ {
		fmt.Printf("%c", b[i])
	}

	flags := b[3] - 32

	data := make([]byte, 3)

	data[0] = b[0] + ((flags & 0x03) << 6)
	data[1] = b[1] + ((flags & 0x0C) << 4)
	data[2] = b[2] + ((flags & 0x30) << 2)

	fmt.Printf(" %X", data)
}

func parseData(b []byte) []byte {
	data := make([]byte, 0)
	for i := 4; i < len(b); i++ {
		if i%4 == 3 {
			if b[i] < 32 || b[i] > (0x5F) {
				fmt.Errorf("Byte out of range: %02x\n", b[i])
			}

			bits := b[i] - 32
			pos := len(data) - 1

			if bits&0x1 != 0 {
				data[pos-2] = data[pos-2] | 0x40
			}
			if bits&0x2 != 0 {
				data[pos-2] = data[pos-2] | 0x80
			}
			if bits&0x4 != 0 {
				data[pos-1] = data[pos-1] | 0x40
			}
			if bits&0x8 != 0 {
				data[pos-1] = data[pos-1] | 0x80
			}
			if bits&0x10 != 0 {
				data[pos] = data[pos] | 0x40
			}
			if bits&0x20 != 0 {
				data[pos] = data[pos] | 0x80
			}
		} else {
			data = append(data, b[i]-0x20)
		}
	}
	return data
}

func printData(b []byte) {
	// fmt.Printf("  %s", string(b[12:]))

	data := parseData(b)

	var address int = (int(data[1]) << 24) + (int(data[2]) << 16) + (int(data[3]) << 8) + int(data[4])

	fmt.Printf(" Address: 0x%08X", address)

	l := len(data)

	last_block := data[l-3:]

	if len(last_block) != 3 {
		fmt.Errorf("Last block length missmatch!")
	}

	fmt.Printf(" last block: %X", last_block)

	fmt.Printf(" -> Last bytes: %02X %02X %02X %02X", b[len(b)-4], b[len(b)-3], b[len(b)-2], b[len(b)-1])

	if last_block[2] == 0x40 && last_block[1] == 0 {
		data = data[:l-3]

		if l-len(data) != 3 {
			fmt.Errorf("Truncate failed!")
		}

	} else if last_block[2] == 0x00 {
		data = data[:l-2]
	} else if last_block[2] == 0x11 {
		data = data[:l-1]
	}

	l = len(data)

	delta := address - prevAddr - prevSize
	prevAddr = address
	prevSize = l - 6

	out.Seek(int64(address), 0)
	out.Write(data[5:l])

	fmt.Printf(" Overlap from previous: %d Data 0: 0x%02X\n", delta, data[0])

}
